import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Default')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @ApiOperation({ summary: 'Return 200 ok if app is running as expected' })
  @ApiResponse({ status: 200, description: 'App is running' })
  getHello(): string {
    return this.appService.getHello();
  }
}
