import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const port = 3001;
  const config = new DocumentBuilder()
    .setTitle('Warm Up Project')
    .setDescription('API Application')
    .setVersion('v1')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(port, () =>
    Logger.log(`Server running at port ${port}`, 'APP'),
  );
}
bootstrap();
